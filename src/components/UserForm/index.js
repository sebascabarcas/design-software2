import React from 'react'
import { Form, Input, Icon, Button, Select, notification, Checkbox, DatePicker } from 'antd'

const FormItem = Form.Item
// const formItemLayout = {
// layout: 'horizontal',
// labelCol: {
//   xs: { span: 24 },
//   sm: { span: 8 },
// },
//   wrapperCol: {
//     xs: { span: 24 },
//     sm: { span: 14 },
//   },
// };

class UserFormComponent extends React.Component {
  state = {
    user: {},
    cities: [],
    departments: []
  }

  componentDidMount () {
    this.getDepartments()
  }

  handleSubmit = e => {
    e.preventDefault()
    const { form } = this.props
    form.validateFields((error, values) => {
      if (!error) {
        const user = values
        user.birthdate.format('YYYY-MM-DD')
        user.update_date.format('YYYY-MM-DD')
        console.log(user);
        this.createUser(user)
      }
    })
    }
     createUser = async (user) => {
        try {
            const new_user = await fetch("http://localhost/clients", {
                method: 'POST', // or 'PUT'
                body: JSON.stringify(user), // data can be `string` or {object}!
                headers:{
                'Content-Type': 'application/json'
                }
              })
              if (new_user) {
                notification.success({
                    message: 'Creación exitosa!',
                    description:
                      'Gracias!',
                  })
              }
        } catch (error) {
            console.log(error);
        }
    }

    getDepartments = async () => {
        const departments = await fetch('http://localhost/departments')
        this.setState({departments: departments.data})
    }

    getCitites = async (id) => {
        console.log(id);
        
        const cities = await fetch('http://localhost/cities?department_id='+id)
        this.setState({cities: cities.data})
    }

    // const { form } = this.props
    // form.validateFields((err, values) => {
    // })


  render() {
    const { form } = this.props
    const { Option } = Select
    const { cities, departments} = this.state
    const departmentOptions = departments.map(departmentOption => (
    <Option key={departmentOption.name} value={departmentOption.id}>
        {departmentOption.name.toUpperCase()}
      </Option>
    ))
    const citiesOptions = cities.map(cityOption => (
    <Option key={cityOption.name} value={cityOption.id}>
        {cityOption.name.toUpperCase()}
      </Option>
    ))
    return (
      <Form layout="horizontal" onSubmit={this.handleSubmit}>
        
        <FormItem
          label="Número de documento"
        >
          {form.getFieldDecorator('identifier', {
            rules: [
              {
                required: true,
                message: 'Por favor ingrese su número de documento!',
                pattern: /[0-9]/g,
              },
            ],
          })(
            <Input
              prefix={<Icon type="idcard" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="text"
              placeholder="Número de documento"
            />,
          )}
        </FormItem>

        <FormItem
          label="Nombre"
        >
          {form.getFieldDecorator('name', {
            rules: [{ required: true, message: 'Por favor ingrese su nombre!' }],
          })(<Input placeholder="Nombre" />)}
        </FormItem>

        <Form.Item label="Birthdate">
          {form.getFieldDecorator('birthdate', {rules: [{ required: true, message: 'Por favor ingrese su fecha de nacimiento!' }]})(<DatePicker placeholder="Escoja fecha de nacimiento" format="YYYY-MM-DD" />)}
        </Form.Item>

        <FormItem
          label="Género"
        >
          {form.getFieldDecorator('gender', {
            rules: [{ required: true, message: 'Por favor escoja un género!' }],
          })(
            <Select>
              <Option value="male">Masculino</Option>
              <Option value="female">Femenino</Option>
            </Select>,
          )}
        </FormItem>
        <FormItem
          >
            {form.getFieldDecorator('department', {
              rules: [{ required: true, message: 'Por favor escoja un departamento!' }],
            })(
              <Select
                placeholder="Seleccione el departamento"
                style={{ width: '100%' }}
                // onFocus={this.handleLocationFocus}
                onSelect={this.getCitites}
                // loading={locations.length === 0}
              >
                {departmentOptions}
              </Select>,
            )}
          </FormItem>
        <FormItem
          >
            {form.getFieldDecorator('city_id', {
              rules: [{ required: true, message: 'Por favor escoja una ciudad!' }],
            })(
              <Select
                placeholder="Seleccione el departamento"
                style={{ width: '100%' }}
                // onFocus={this.handleLocationFocus}
                // onSelect={this.handleLocationSelect}
                // loading={locations.length === 0}
              >
                {citiesOptions}
              </Select>,
            )}
          </FormItem>
       
        <FormItem
          label="E-mail"
        >
          {form.getFieldDecorator('email', {
            rules: [{ required: true, message: 'Por favor ingrese su correo!' }],
          })(
            <Input
              prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
              placeholder="Email"
            />,
          )}
        </FormItem>
        
        

        <FormItem
          label="Número de contacto"
        >
          {form.getFieldDecorator('phone', {
            rules: [
              {
                required: true,
                message: 'Por favor ingrese su número de contacto!',
                pattern: /[0-9]/g,
              },
            ],
          })(
            <Input
              prefix={<Icon type="phone" style={{ color: 'rgba(0,0,0,.25)' }} />}
              type="text"
              placeholder="Número de contacto"
            />,
          )}
        </FormItem>
        <FormItem
          label="Address"
        >
          {form.getFieldDecorator('address', {
            rules: [{ required: true, message: 'Por favor ingrese su dirección!' }],
          })(<Input placeholder="Dirección" />)}
        </FormItem>
        
        
        <Form.Item label="Update Date">
          {form.getFieldDecorator('update_date', {rules: [{ required: true, message: 'Por favor ingrese la fecha de actualización!' }]})(<DatePicker placeholder="Escoja fecha de actualización" format="YYYY-MM-DD"/>)}
        </Form.Item>
        <Form.Item label="Terminos y Condiciones">
        {form.getFieldDecorator('accepts_data_processing_policy', {
            valuePropName: 'checked',
            rules: [{required: true, message: 'Por favor acepte terminos y condiciones!'}]
          })(
            <Checkbox>
              Acepto terminos y condiciones
            </Checkbox>,
          )}
        </Form.Item>
        {/* <FormItem validateStatus="validating">
          {form.getFieldDecorator('second_last_name', {
            // rules: [{ required: true, message: 'Por favor ingrese su segundo apellido!' }],
          })(<Input placeholder="Segundo apellido" />)}
        </FormItem> */}

        <div className="form-actions">
          <Button type="primary" htmlType="submit" className="login-form-button">
            Crear
          </Button>
        </div>
      </Form>
    )
  }
}

const UserForm = Form.create()(UserFormComponent)
export default UserForm
