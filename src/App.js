import React from 'react';
import logo from './logo.svg';
import {Input, Icon, Button, Select, notification, Checkbox, Row, DatePicker} from 'antd';
import {useState, useEffect, withRenderProps} from 'react';
import './App.css';
import UserForm from './components/UserForm';

const {Option} = Select;

function App () {
  const [loading, _setLoading] = useState (false);
  const [cities, _setCities] = useState (false);
  const [departments, _setDepartments] = useState (false);
  const user = {};

  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <UserForm/>
      </header>
    </div>
  );
}

export default App;
